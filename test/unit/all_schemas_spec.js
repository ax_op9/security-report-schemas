import b from './builders/index'
import {schemas} from './support/schemas'
import {withVulnerabilityDetails} from './builders/vulnerability_details/vulnerability'

const schemaTypes = [
  {name: 'dast', builder: b.dast, schema: schemas.dast},
  {name: 'sast', builder: b.sast, schema: schemas.sast},
  {name: 'container_scanning', builder: b.container_scanning, schema: schemas.container_scanning},
  {name: 'coverage_fuzzing', builder: b.coverage_fuzzing, schema: schemas.coverage_fuzzing},
  {name: 'secret_detection', builder: b.secret_detection, schema: schemas.secret_detection},
  {name: 'dependency_scanning', builder: b.dependency_scanning, schema: schemas.dependency_scanning}
]


schemaTypes.forEach((type) => {
  describe(`common[${type.name}] schema`, () => {
    it('allows ftp and http schemes for identifiers[].url', () => {
      const report = type.builder.report({
        vulnerabilities: [type.builder.vulnerability({
          identifiers: [{
            type: "type",
            name: "name",
            value: "value",
            url: "https://cwe.mitre.org/data/definitions/362.html"
          },
          {
            type: "type",
            name: "name",
            value: "value",
            url: "ftp://cwe.mitre.org/data/definitions/362.html"
          }]
        })]
      })

      expect(type.schema.validate(report).success).toBeTruthy();
    })

    it('should not validate when there are no identifiers', () => {
      const report = type.builder.report({
        vulnerabilities: [type.builder.vulnerability({
          identifiers: []
        })]
      })

      expect(type.schema.validate(report).errors[0].message).toContain('must NOT have fewer than 1 items')
    })

    it('should not validate with empty identifier[].type', () => {
      const report = type.builder.report({
        vulnerabilities: [type.builder.vulnerability({
          identifiers: [{
            type: "",
            name: "name",
            value: "value"
          }]
        })]
      })

      expect(type.schema.validate(report).errors[0].message).toContain('must NOT have fewer than 1 characters')
    })

    it('should not validate with empty identifier[].name', () => {
      const report = type.builder.report({
        vulnerabilities: [type.builder.vulnerability({
          identifiers: [{
            type: "type",
            name: "",
            value: "value"
          }]
        })]
      })

      expect(type.schema.validate(report).errors[0].message).toContain('must NOT have fewer than 1 characters')
    })

    it('should not validate with empty identifier[].value', () => {
      const report = type.builder.report({
        vulnerabilities: [type.builder.vulnerability({
          identifiers: [{
            type: "type",
            name: "name",
            value: ""
          }]
        })]
      })

      expect(type.schema.validate(report).errors[0].message).toContain('must NOT have fewer than 1 characters')
    })

    it('must have a version', () => {
      const report = type.builder.report({version: undefined})
      expect(type.schema.validate(report).errors[0].message).toContain('must have required property \'version\'')
    })

    it('can have zero vulnerabilities', () => {
      const report = type.builder.report({vulnerabilities: []})
      expect(type.schema.validate(report).success).toBeTruthy()
    })

    it('validates with additional details about vulnerabilities', () => {
      const report = withVulnerabilityDetails(type.builder.report());
      expect(type.schema.validate(report).success).toBeTruthy();
    });

    it('validates source tracking type', () => {
      const report = type.builder.report({
        vulnerabilities: [type.builder.vulnerability({
          tracking: {
            type: "source",
            items: [{
              file: "app/controllers/groups_controller.rb",
              start_line: 6,
              end_line: 6,
              signatures: [{
                "algorithm": "scope_offset",
                "value": "app/controllers/groups_controller.rb|GroupsController[0]|new_group[0]:4"
              }]
            }]
          }
        })]
      })

      expect(type.schema.validate(report).success).toBeTruthy();
    })

    it('validates flags type', () => {
      const report = type.builder.report({
        vulnerabilities: [type.builder.vulnerability({
            flags: [{
                type: "flagged-as-likely-false-positive",
                origin: "post analyzer X",
                description: "static string to sink"
            },{
                type: "flagged-as-likely-false-positive",
                origin: "post analyzer Y",
                description: "integer to sink"
            }]
        })]
      })

      expect(type.schema.validate(report).success).toBeTruthy();
    })

    it('complies to the JSON specification', () => {
      const report = type.builder.report()
      expect(schemas.json_draft_07.validate(report).success).toBeTruthy()
    })

    it('should not validate invalid scan options', () => {
      const report = type.builder.report({
        scan: type.builder.scan({
          options: [
            { /* name < minLength */ value: "valid value", name: "" },
            { /* name > maxLength */ value: "valid value", name: "THIS_NAME_IS_LONGER_THAN_THE_LEGAL_LIMIT_FOR_SCAN_OPTIONS_SINCE_THE_CHARACTER_LIMIT_IS_ONLY_TWO_HUNDRED_FIFTY_FIVE_CHARACTERS_BUT_THIS_IS_A_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_LONG_STRING" },
            { name: "source_empty", source: "", value: "valid value" },
            { name: "undefined_enum_source", source: "invalid source", value: "valid value" },
            { name: "value_array", value: [] },
            { name: "value_object", value: {} },
            { name: null, value: "valid value" },
          ]
        })
      })

      expect(type.schema.validate(report).success).toBeFalsy();
      expect(type.schema.validate(report).errors.length).toBe(7)
      expect(type.schema.validate(report).errors[0].message).toContain('must NOT have fewer than 1 characters')
      expect(type.schema.validate(report).errors[1].message).toContain('must NOT have more than 255 characters')
      expect(type.schema.validate(report).errors[2].message).toContain('must be equal to one of the allowed values')
      expect(type.schema.validate(report).errors[3].message).toContain('must be equal to one of the allowed values')
      expect(type.schema.validate(report).errors[4].message).toContain('must be boolean,integer,null,string')
      expect(type.schema.validate(report).errors[5].message).toContain('must be boolean,integer,null,string')
      expect(type.schema.validate(report).errors[6].message).toContain('must be string')
    })

    it('should not validate null scan options', () => {
      const report = type.builder.report({
        scan: type.builder.scan({
          options: null
        })
      })

      expect(type.schema.validate(report).success).toBeFalsy();
      expect(type.schema.validate(report).errors[0].message).toContain('must be array')
    })


    it('validates empty scan options', () => {
      const report = type.builder.report({
        scan: type.builder.scan({
          options: []
        })
      })

      expect(type.schema.validate(report).success).toBeTruthy();
    })

    it('validates undefined scan options', () => {
      const report = type.builder.report({
        scan: type.builder.scan({
          options: undefined
        })
      })

      expect(type.schema.validate(report).success).toBeTruthy();
    })

    it('validates valid scan options', () => {
      const report = type.builder.report({
        scan: type.builder.scan({
          options: [
            { name: "bool", source: "argument", value: true },
            { name: "empty_string", source: "env_variable", value: "" },
            { name: "int", source: "env_variable", value: 2 },
            { name: "nullable", source: "other", value: null },
            { name: "string", source: "file", value: "fatal" },
          ]
        })
      })

      expect(type.schema.validate(report).success).toBeTruthy();
    })

    it('validates scan analyzer', () => {
      const report = type.builder.report({
        scan: type.builder.scan({
          analyzer: {
            id: 'gitlab-dast',
            name: 'GitLab DAST',
            url: 'https://gitlab.com/dast',
            version: '1.6.20',
            vendor: {
              name: 'GitLab'
            }
          }
        })
      })

      expect(type.schema.validate(report).success).toBeTruthy();
    })

    it('should not validate when vulnerability[].name exceeds maxLength', () => {
      const report = type.builder.report({
        vulnerabilities: [type.builder.vulnerability({
          name: 'n'.repeat(255+1)
        })]
      })

      expect(type.schema.validate(report).errors[0].message).toContain('must NOT have more than 255 characters')
    })

    it('should not validate when vulnerability[].description exceeds maxLength', () => {
      const report = type.builder.report({
        vulnerabilities: [type.builder.vulnerability({
          description: 'd'.repeat(1048576+1)
        })]
      })

      expect(type.schema.validate(report).errors[0].message).toContain('must NOT have more than 1048576 characters')
    })

    it('should not validate when vulnerability[].solution exceeds maxLength', () => {
      const report = type.builder.report({
        vulnerabilities: [type.builder.vulnerability({
          solution: 's'.repeat(7000+1)
        })]
      })

      expect(type.schema.validate(report).errors[0].message).toContain('must NOT have more than 7000 characters')
    })

    it('should validate remediations when required properties are present', () => {
      const report = type.builder.report({
        remediations: [
          {
            fixes: [{id: 'id'}, {id: 'other-id'}],
            summary: 'Remediation summary',
            diff: 'a diff'
          }
        ]
      })

      expect(type.schema.validate(report).success).toBeTruthy();
    })

    it('should not validate remediations when fixes[].id is missing', () => {
      const report = type.builder.report({
        remediations: [
          {
            fixes: [{cve: 'some-cve'}],
            summary: 'Remediation summary',
            diff: 'a diff'
          }
        ]
      })

      expect(type.schema.validate(report).errors[0].message).toContain('must have required property \'id\'')
    })
  })
})

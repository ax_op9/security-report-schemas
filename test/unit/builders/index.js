import dast from './dast/index'
import sast from './sast/index'
import dependency_scanning from './dependency_scanning/index'
import secret_detection from './secret_detection/index'
import cluster_image_scanning from './cluster_image_scanning/index'
import container_scanning from './container_scanning/index'
import coverage_fuzzing from './coverage_fuzzing/index'

export default {
  dast,
  sast,
  dependency_scanning,
  secret_detection,
  cluster_image_scanning,
  container_scanning,
  coverage_fuzzing
}

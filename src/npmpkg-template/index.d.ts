// Type definitions for security-report-schemas@v${pkg_version}
// Project: ${project_url}
// Definitions by: GitLab Org <https://gitlab.com/gitlab-org/>


/** Report schema used for Cluster Image Scanning results */
export const ClusterImageScanningReportSchema: typeof import('./dist/cluster-image-scanning-report-format.json');

/** Report schema used for Container Scanning results */
export const ContainerScanningReportSchema: typeof import('./dist/container-scanning-report-format.json');

/** Report schema used for Coverage Fuzzing results */
export const CoverageFuzzingReportSchema: typeof import('./dist/coverage-fuzzing-report-format.json');

/** Report schema used for Dynamic Application Security Testing (DAST) results */
export const DastReportSchema: typeof import('./dist/dast-report-format.json');

/** Report schema used for Dependency Scanning results */
export const DependencyScanningReportSchema: typeof import('./dist/dependency-scanning-report-format.json');

/** Report schema used for Static Application Security Testing (SAST) results */
export const SastReportSchema: typeof import('./dist/sast-report-format.json');

/** Report schema used for Secret Detection results */
export const SecretDetectionReportSchema: typeof import('./dist/secret-detection-report-format.json');


/** All security report schemas */
export namespace schemas {

    /** Report schema used for Cluster Image Scanning results */
    const cluster_image_scanning: typeof ClusterImageScanningReportSchema
    
    /** Report schema used for Container Scanning results */
    const container_scanning: typeof ContainerScanningReportSchema;
    
    /** Report schema used for Coverage Fuzzing results */
    const coverage_fuzzing: typeof CoverageFuzzingReportSchema;
    
    /** Report schema used for Dynamic Application Security Testing (DAST) results */
    const dast: typeof DastReportSchema;
    
    /** Report schema used for Dependency Scanning results */
    const dependency_scanning: typeof DependencyScanningReportSchema;
    
    /** Report schema used for Static Application Security Testing (SAST) results */
    const sast: typeof SastReportSchema;

    /** Report schema used for Secret Detection results */
    const secret_detection: typeof SecretDetectionReportSchema;
}

export default schemas;
